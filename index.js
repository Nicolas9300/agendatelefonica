const express = require('express')

const app = express()
app.use(express.json())

const agenda = [

   {id:1,
    name: "Arto Hellas",
    number: "040-123456"
   },
   {id:2,
    name: "Ada Lovelace",
    number: "39-44-5323523"
   },
   {id:3,
    name: "Dan Abramov",
    number: "12-43-234345"
   },
   {id:4,
    name: "Mary Poppendick",
    number: "39-23-6423122"
   }
]

app.get('/api/persons',(req,res)=>{
   res.json(agenda)
})

app.get('/api/persons/:id',(req,res)=>{
    const {id} = req.params
    const person = agenda.find(person => person.id === Number(id))
    if(person){ res.json(person)}
    else res.status(404).end()
})

app.delete('/api/persons/:id',(req,res)=>{
    const {id} = req.params
    const resultPersons = agenda.filter(person => person.id !== Number(id))
    console.log(resultPersons) 
    res.status(204).end()
})

app.get('/info',(req,res)=>{
    const date = new Date()
    res.send(`Phonebook has info for ${agenda.length} people<br/>` + `<br/>${date.getUTCDate()}`)
})

app.post('/api/persons/',(req,res)=>{
    const newPerson = req.body

    const resultPersons =  agenda.concat(newPerson)
    console.log(resultPersons)
    res.status(201).json(newPerson)
})



app.listen(3000,()=>{
    console.log('Server running on Port '+3000)
})